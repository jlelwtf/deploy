#!/bin/bash

cd /root

if [ -z $1 ]; then
  echo "Не указана версия"
  exit 1
fi

VERSION=$1

IP_MANAGER_NODE="95.217.239.216"

EMBEDDER_SERVICE_NAME="embedder"
EMBEDDER_ADDRESS="${IP_MANAGER_NODE}:8500"
EMBEDDER_URL="http://${IP_MANAGER_NODE}:8501/v1/models/use:predict"
EMBEDDER_HEALTHCHECK="http://${IP_MANAGER_NODE}:8501/v1/models/use"
EMBEDDER_MODEL_PATH="/root/models/use"

DATA_DIR="/root/data"
CONTAINER_DATA_DIR="/data"
EMBEDDINGS_FILE="embeddings/${VERSION}/embeddings.pkl"
INDEX_FILE="indices/${VERSION}/index.json"
CLUSTERS_CENTERS_FILE="clusters_centers/${VERSION}/clusters_centers.pkl"

INDEX_APP_NAME="index_app_${VERSION}"
INDEX_APP_PORT="$((8000 + $VERSION))"
INDEX_APP_REPLICAS=1

GW_APP_NAME="api_gw_${VERSION}"
GW_APP_PORT=7000
GW_APP_PORT_V="$((7000 + $VERSION))"
GW_APP_REPLICAS=1

# Получаем версию приложения, которое сейчас запущено (если не запущено, то будет null)
CURR_VERSION=$( curl -s http://${IP_MANAGER_NODE}:${GW_APP_PORT}/version | jq '.version' )

if [[ ${CURR_VERSION} == ${VERSION} ]] ; then
  echo "${VERSION} версия уже запущена"
  exit 0
fi

echo "_________________________________________________________"
# Запуск модели в TF Serving

docker service inspect $EMBEDDER_SERVICE_NAME &> /dev/null
if [ $? -ne 0 ]; then
	echo "Поднимаем эмбеддер"
	docker service create -d --name $EMBEDDER_SERVICE_NAME\
    -p 8501:8501 -p 8500:8500\
	  --mount type=bind,src=${EMBEDDER_MODEL_PATH},dst=/models/use\
	  -e MODEL_NAME=use\
	  tensorflow/serving

	 # Проверяем поднялся ли эмбеддер
  for itr in {1..3}
    do
      echo "Ждем 10 секунд"
      sleep 10
      echo "${itr}. Пробуем дернуть эмбеддер"
      curl -X GET ${EMBEDDER_HEALTHCHECK} &> /dev/null

      if [ $? -ne 0 ]; then
        echo "   Запрос не прошел"
        if [ ${itr} == 3 ]; then
          echo "Что-то пошло не так, эмбеддер так и не поднялся"
          exit 1
        fi
      else
        echo "Запрос прошел. Эмбеддер поднят"
        break
      fi
    done
else
	echo "Эмбеддер уже поднят"
fi


echo "_________________________________________________________"

cd index_app

# Проверка консистентности данных
python3 check_consistency.py \
  --clusters_centers_path ${DATA_DIR}/${CLUSTERS_CENTERS_FILE} \
  --index_path ${DATA_DIR}/${INDEX_FILE}

if [ $? -ne 0 ]; then
  echo "Проверка консистентности данных не прошла !!!"
  exit 1
else
  echo "Проверка консистентности данных прошла"
fi

# Расчет эмбеддингов для предложений в кластерах (предполагается, что файлы с кластерами уже заргужены)

if [ -f "$DATA_DIR/$EMBEDDINGS_FILE" ]; then
	echo "Эмбеддинги уже рассчитаны"
else
	echo "Расчет эмбеддингов"

	python3 embedding_loader.py --embedder_address=$EMBEDDER_ADDRESS\
	 --embeddings_path=${DATA_DIR}/${EMBEDDINGS_FILE}\
	 --index_path=${DATA_DIR}/${INDEX_FILE}

  if [ $? -ne 0 ]; then
    echo "При расчете эмбеддингов произошла ошибка"
    exit 1
  fi
	echo "Done"
fi
# todo: раскатать данные на все ноды кластера

cd ..

echo "_________________________________________________________"
# todo собрать образ перед запуском

# Запуск индекса
docker service inspect ${INDEX_APP_NAME} &> /dev/null
if [ $? -ne 0 ]; then
	echo "Поднимаем сервис индекса"

	docker service create -d --name ${INDEX_APP_NAME} \
        -p ${INDEX_APP_PORT}:5000 \
        --replicas ${INDEX_APP_REPLICAS} \
        --mount type=bind,src=${DATA_DIR},target=${CONTAINER_DATA_DIR} \
        --env INDEX_PATH=${CONTAINER_DATA_DIR}/${INDEX_FILE} \
        --env EMBEDDING_PATH=${CONTAINER_DATA_DIR}/${EMBEDDINGS_FILE} \
        --env EMBEDDER_ADDRESS=${EMBEDDER_ADDRESS} \
        localhost:6000/index_app

  # Проверяем поднялся ли индекс
  for itr in {1..3}
  do
    echo "Ждем 10 секунд"
    sleep 10
    echo "${itr}. Пробуем дернуть /health у сервиса индекса"
    curl -X GET http://${IP_MANAGER_NODE}:${INDEX_APP_PORT}/health &> /dev/null
    healthcheck=$?
    if [ ${healthcheck} -ne 0 ]; then
      echo "   Запрос не прошел"
      if [ ${itr} == 3 ]; then
        echo "Что-то пошло не так, сервис индекса так и не поднялся"
        exit 1
      fi
    else
      echo "Запрос прошел. Сервис поднят"
      break
    fi
  done

else
	echo  "Сервис индекса уже поднят"
fi
echo "_________________________________________________________"

# Запуск api gw
docker service inspect ${GW_APP_NAME} &> /dev/null
if [ $? -ne 0 ]; then
	CURRENT_API_GW_APP=api_gw_${CURR_VERSION}
	# Проверяем поднята ли сейчас какая-либо версия
	docker service inspect ${CURRENT_API_GW_APP} &> /dev/null
	GW_ISN_UP=$?
	if [ ${GW_ISN_UP} -ne 0 ]; then
    PORT=${GW_APP_PORT}
	else
		PORT=${GW_APP_PORT_V}
	fi
	echo "Поднимаем сервис api"
  docker service create -d --name ${GW_APP_NAME} \
    -p ${PORT}:5000 \
    --replicas ${GW_APP_REPLICAS} \
    --mount type=bind,src=${DATA_DIR},target=${CONTAINER_DATA_DIR} \
    --env EMBEDDER_URL=${EMBEDDER_URL} \
    --env INDEX_URL="http://${IP_MANAGER_NODE}:${INDEX_APP_PORT}/get_k_nearest" \
    --env CLUSTERS_CENTERS_PATH=${CONTAINER_DATA_DIR}/$CLUSTERS_CENTERS_FILE \
    --env VERSION=$VERSION \
    localhost:6000/api_gw

  # Проверяем поднялся ли gw
  for itr in {1..3}
  do
    echo "Ждем 10 секунд"
    sleep 10
    echo "${itr}. Пробуем дернуть /health у сервиса индекса"
    curl -X GET http://${IP_MANAGER_NODE}:${PORT}/health &> /dev/null

    if [ $? -ne 0 ]; then
      echo "   Запрос не прошел"
      if [ ${itr} == 3 ]; then
        echo "Что-то пошло не так, сервис gw так и не поднялся"
        exit 1
      fi
    else
      echo "Запрос прошел. Сервис поднят"
      break
    fi
  done

  # Проверяем корректно ли отрабаытывает запрос
  status_code=$(curl --write-out %{http_code} --silent \
    --output /dev/null "http://${IP_MANAGER_NODE}:${PORT}/get_answer?question=hello")
  if [ $status_code -ne 200 ]; then
    echo "!!! Тестовый запрос отработал некорректно !!!"
    if [ $GW_ISN_UP -eq 0 ]; then
      echo "Порт не будет перключен на новую версию"
    fi
    exit 1
  else
    echo "Тестовый запрос отработал корректно."
  fi

	if [ $GW_ISN_UP -eq 0 ]; then
	  # Переключаем порты у старой версии gw 7000 + Version
	  # у новой версии на 7000
		docker service update --publish-rm ${GW_APP_PORT}:5000 \
		  --publish-add "$(($GW_APP_PORT + $CURR_VERSION))":5000 \
		  ${CURRENT_API_GW_APP}

		docker service update --publish-rm ${GW_APP_PORT_V}:5000 \
		  --publish-add ${GW_APP_PORT}:5000 \
		  ${GW_APP_NAME}

		echo "Порт переключен на новую версию"
	fi

else
  echo  "Сервис api уже поднят"
fi
